import json
import os
import pdb
import grammar2
import utilities
from merge_config_files import allowed_duplicates_any_section, allowed_duplicates


### here we define what is allowed
allowed_keys_in_all_sections = [
    'distributed',
    'LoadCtrlLibs',
    'dataPort',
    'data',
    'eventPort',
    'event',
    'queryFunction',
    'maxDpNamesCount',
    'ctrlMaxPendings',
    'aliveTimeout',
    'IOTransitionTimeout']
allowed_keys = {
    'general': [
        'pvss_path',
        'proj_path',
        'proj_version',
        'distributed',
        'pmonPort',
        'password',
        'userName',
        'maxConnectMessageSize',
        'langs',
        'useRDBArchive',
        'useRDBGroups',
        'dpFuncLoopCount',
        'activateAlertFiltering'],
    'dist': [
        'distPeer',
        'distPort'],
    'ui': [
        'LoadCtrlLibs',
        'aesShowDistDisconnections',
        'visionResizeMode',
        'queryRDBdirect',
        'CtrlDLL',
        'useElemNameForReadRequest'],
    'opcua': [
        'certificateStore',
        'server',
        'smoothBit',
        'maxRequestQueueSize',
        'setInvalidForConnLoss'],
    'ctrl': [
        'queryRDBdirect',
        'CtrlDLL'],
    'event': [],
    'opc': [
        'server',
        'smoothBit',
        'ioTimeout',
        'addItemsSingle'],
    'data': [
        'dataBgName',
        'smoothBit'],
    'valarch': [
        'deleteNotArchivedFileSets'],
    'ValueArchiveRDB': [
        'queryOverBounds',
        'bufferToDisk',
        'DbType',
        'Db',
        'DbUser',
        'DbPass',
        'maxRequestThreads',
        'writeWithBulk',
        'sendMaxTS',
        'queryFunction'],
    'dip':[
        'DipConfig'],
    'mod':[
        'plc',                  
        'tcpServerPort',
        'maxQueueSize']
}
###

ERR_CONFIG_OPEN = -1
ERR_ILLEGAL_KEY = -3
ERR_KEY_CASING = -5
ERR_SYNTAX = -2
ERR_DUPLICATE = -4

color_map = {
    ERR_CONFIG_OPEN: 'darkred',
    ERR_SYNTAX: 'red',
    ERR_ILLEGAL_KEY: 'orange',
    ERR_KEY_CASING: 'violet',
    ERR_DUPLICATE: 'navy'
}

def always_say_yes(q):
    return True

def get_proj_config_path(subdet, proj):

    
    if proj in ['ATLFWDSCS','ATLCALSCS','ATLIDGSCS']:
        subdet = proj[3:6]
    file_name = 'configs/'+proj+'.config'
    if not os.path.isfile(file_name):
        # try to download it
        print 'Downloading '+file_name
        svn_path = 'svn+ssh://svn.cern.ch/reps/atlasdcs/ATLAS_DCS_{0}/trunk/{1}/config/config'.format(subdet, proj)
        print 'SVN path: '+svn_path
        os.system('svn export '+svn_path+' configs/'+proj+'.config')
    return file_name

        
def lines(filen):
    f = file(filen, 'r')
    return f.readlines()

def check_duplicates(config, result_list):
    for section in config.sections:
        section_name = section.name
        if section_name.find('_') >= 0:  # treat sections with driver number suffix
            section_name = section_name[:section_name.find('_')]
        allowed_duplicates_list = allowed_duplicates_any_section
        key = section_name
        if key in allowed_duplicates:
            allowed_duplicates_list += allowed_duplicates[key] 
        (ok, duplicates) = utilities.check_duplicates(section, allowed_duplicates_list)
        if not ok:
            result_list.append( (project, ERR_DUPLICATE, 'illegal duplicate(s) in section ['+section.name.name+']: '+','.join(duplicates)  ) ) 


def validate_project_config(host_name, project, result_list, allowed_keys):
    print host_name
    subdet = host_name[5:8]
    print subdet
    # proj = host_name[2:]
    # print proj
    config_path = get_proj_config_path(subdet, project)
    try:
        configuration = grammar2.open_configuration(config_path, always_say_yes)
    except Exception as e:
        if type(e) == SyntaxError:
            lineno = e.lineno
            contents = lines(config_path)[lineno-1:lineno+9]
            extra_msg = ''
            if '#begin' in contents[0]:
                extra_msg = '<br>Problem seems related to unmatched #begin; NOTE #begin usage is reserved to fwComponents!<br>'
            result_list.append((project, ERR_SYNTAX, str(e)+extra_msg+'<br>10 lines follow:<br>'+'<br>'.join( contents )))
            return
        else:
            result_list.append((project, ERR_CONFIG_OPEN, str(e)))
            return
    # step 2: dictionary check
    for section in configuration.sections:
        section_name = section.name
        if section_name.find('_') >= 0:  # treat sections with driver number suffix
            section_name = section_name[:section_name.find('_')]
        if section_name not in allowed_keys.keys():
            print 'WARNING Section ['+section_name+'] is not covered by allowed_keys. Not validating.'
            continue
        kc = utilities.KeyCounter()
        grammar2.visit_all( section, kc )
        for key in kc.result().keys():
            if key not in allowed_keys[section_name]:
                # let's see if thats just casing or simply wrong key ...
                if key.upper() in map(lambda x: x.upper(), allowed_keys[section_name]):
                    result_list.append((project, ERR_KEY_CASING, '[{0}] key casing error: {1}'.format(section.name, key)))
                else:
                    result_list.append((project, ERR_ILLEGAL_KEY, '[{0}] illegal key: {1}'.format(section.name, key)))
    # step 3: duplicate check
    check_duplicates(configuration, result_list)
        
        


    
grammar2.initialize_grammar()

for key in allowed_keys_in_all_sections:
    for section in allowed_keys:
        allowed_keys[section].append(key)

filename = "atlasdcsp1new.json"

result_list = []

with open(filename) as data_file:
    description = json.load(data_file)
for host_name in description:
    projects = description[host_name].keys()
    for project in projects:
        validate_project_config (host_name, project, result_list, allowed_keys)

key_function = lambda y: y[1]
result_list = sorted(result_list, key=key_function, reverse=True)


report = open('validation.html','w')
report.write('<html><body><table border=2>')
for proj_result in result_list:
    print proj_result
    report.write('<tr><td>{0}</td><td bgcolor={3}>{1}</td><td>{2}</td></tr>\n'.format(proj_result[0], proj_result[1], proj_result[2], color_map[proj_result[1]] ) )

report.write('</table>\n total: {0} errors.'.format(len(result_list)))
