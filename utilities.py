import grammar2
import pdb

class KeyCounter():
    """ Intention: KeyCounter can be used as work function for visitor. The outputs would be a dictionary with numbers of occurences of all approached keys  """
    def __init__(self):
        self.buckets = {}
    def __call__(self,x):
        if type(x) == grammar2.SettingLine:
            key = x.name.name  # first name stripts to a Symbol level, second one to string level
            if key in self.buckets:
                self.buckets[key] += 1
                
            else:
                self.buckets[key] = 1
    def result(self):
        return self.buckets

class KeyValueCounter():
    """ Intention: KeyValueCounter can be used as work function for visitor. The outputs would be a dictionary mapping (key,value) into number of occurencies.
        """
    def __init__(self):
        self.buckets = {}
        self.same_key_value = []  # this one is a list of tuples of (list, element)

    def __call__(self,x,l):
        if type(x) == grammar2.SettingLine:
            key = (x.name.name,x.arg_value)  # first name stripts to a Symbol level, second one to string level
            if key in self.buckets:
                self.buckets[key] += 1
                self.same_key_value.append( (l,x) )
            else:
                self.buckets[key] = 1
    def result(self):
        return self.buckets



    

def check_duplicates(section, allowed_duplicates):
    ok = True
    kvc = KeyValueCounter()
    grammar2.visit_all_store_container(section, kvc, None)

    # here we remove "safe" duplicated, i.e. same key-value pairs within given section
    # as per: https://its.cern.ch/jira/browse/ACDCS-1523
    for (l,x) in kvc.same_key_value:
        print('In section ['+str(section.name)+']: now im removing element of name '+str(x.name.name)+' and value '+str(x.arg_value)+' from parent list because it is a safe duplicate (as per ACDCS-1523)')
        l.remove(x)

    keys = {}
    duplicated_keys = []
    for key_statistic in kvc.result():
        if key_statistic[0] in keys:
            keys[key_statistic[0]] += 1
        else:
            keys[key_statistic[0]] = 1
    for key in keys:
        if keys[key] > 1:
            if not key in allowed_duplicates:
                print('ERROR: Key '+str(key)+' is duplicated (with different values) AND Stefan decided that this key is not on the exemption list. Aborting.')
                ok = False
                duplicated_keys.append(key)
    return (ok, duplicated_keys)
