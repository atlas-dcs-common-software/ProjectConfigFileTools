#!/usr/bin/python3

import grammar2
import sys
import pypeg2
import pdb
import argparse
import utilities
import re

grammar2.initialize_grammar()

# DUPLICATED HANDLING
# To see which duplicates are allowed, make an OR from allowed_duplicates(per section) with allowed_duplicates_any_section

allowed_duplicates = {
    'general' : ['proj_path'],
    'dist' :    ['distPeer'],
    'TGC'  :    ['Bus'],
    'mod'  :    ['plc'],
    'opcua':    ['server'],
    'ui'   :    ['scriptEditorParseLibs']
    }

# The following will match sections with driver number appended,
# e.g. for key TGC will match TGC_xxx where xxx is a positive integer
allowed_duplicates_per_manager = {
    'TGC'  :    ['Bus']
    }

allowed_duplicates_any_section = ['LoadCtrlLibs', 'CtrlDLL', 'smoothBit']

verbose = False

regex_driver_section_name = re.compile(r"(?P<driver_name>\S+)_\d+")

def yes_or_no(question):
    while True:
        yn = input(question+' type y or n; then enter   ')
        if yn in ['y','n']:
            return yn=='y'
def writeout(cfg, fn):
    out = pypeg2.compose(cfg, autoblank=False)
    out_f = open(fn, 'w+')
    out_f.write(out)
    out_f.close()


def section_has_worthy_contents(section):
    for element in section.contents.elements:
        if type(element) in [grammar2.SettingLine, grammar2.CommentBlock]:
            return True
    return False

def remove_given_keys_from_section(section, keys):
    to_remove = []
    for element in section.contents.elements:
        if type(element) == grammar2.SettingLine:
            if str(element.name) in keys:
                to_remove.append(element)
    for element in to_remove:
        print('From ['+section.name+'] removing setting with key: '+str(element.name))
        section.contents.elements.remove(element)

def remove_all_trailing_comments(cfg):
    # find all SettingLine elements
    for section in cfg.sections:
        # find SettingLines directly in sections - not necessary to do so in blocks because blocks are removed already
        for element in section.contents.elements:
            if type(element) == grammar2.SettingLine:
                if element.comment:
                    print('In section ['+section.name+'] dropping trailing comment: "' + element.comment + '"')
                    element.comment = None
                # remove trailing whitespace
                element.arg_value = element.arg_value.rstrip()

def warn_control_lib_loaded(cfg):
    for section in cfg.sections:
        if section.name == 'event': continue
        for element in section.contents.elements:
            if type(element) == grammar2.SettingLine:
                if str(element.name) == 'LoadCtrlLibs':
                    print('WARNING: control lib loaded in section ['+section.name+']; lib='+element.arg_value+'  This is NOT RECOMMENDED: libs should be loaded with #uses')



def warn_if_removing_non_standard_component(name):
    standard_name = re.compile(r"(fw.*)|unDistributedControl")
    if not standard_name.match(name):
        print('WARNING removing component entry name '+str(name)+' even though it has non-standard name!')

def fix_path(input):
    ''' Removes multiple (2 or more) slashes in the path. Also drops trailing slash(es) if present. '''
    single_slashes = re.sub(r'/{2,}', '/', input)
    no_trailing_slashes = re.sub(r'/*"$','"',single_slashes)
    return no_trailing_slashes

def preprocess_old_config(cfg):
    # remove all #begin..#end blocks

    all_sections = cfg.sections
    for section in all_sections:
        blocks = section.all_blocks()
        for block in blocks:
            warn_if_removing_non_standard_component(block.component_name_begin)
            section.contents.elements.remove(block)


    # if there is any empty section, remove it as well
    print('Removing sections with no worthy contents from old config file')
    sections_to_remove=[]
    #pdb.set_trace()
    for section in cfg.sections:
        if not section_has_worthy_contents(section):
            sections_to_remove.append(section)
    for section in sections_to_remove:
        print('Removing whole section ['+section.name+'] because it got empty after pre-processing')
        cfg.sections.remove(section)

    general_section = cfg.section_by_name('general')
    remove_given_keys_from_section(general_section, ['pvss_path', 'proj_version', 'userName', 'password', 'langs', 'distributed', 'useRDBArchive', 'useRDBGroups' ])
    # look at proj_path, get rid of one with fwInstallation
    to_remove = []
    general_proj_paths = [x for x in general_section.contents.elements if type(x) == grammar2.SettingLine and str(x.name) == 'proj_path']
    for proj_path in general_proj_paths:
        if 'fwInstallation' in proj_path.arg_value:
            to_remove.append( proj_path )
    for x in to_remove:
        print('Removing fwInstallation proj_path: '+x.arg_value)
        general_section.contents.elements.remove(x)


    # From ValueArchiveRDB, zero-out DbPass if it is specified
    value_arch_rdb_section = cfg.section_by_name('ValueArchiveRDB')
    if value_arch_rdb_section:
        for element in value_arch_rdb_section.contents.elements:
            if type(element) == grammar2.SettingLine and str(element.name) == 'DbPass':
                print('WARNING: I set DbPass value in ValueArchiveRDB section to empty string')
                element.arg_value = '""'

    remove_all_trailing_comments(cfg)

    warn_control_lib_loaded(cfg)

def find_setting_line(cfg, section_name, key):
    section = cfg.section_by_name(section_name)
    if not section:
        return None
    for element in section.contents.elements:
        if type(element) == grammar2.SettingLine and str(element.name) == key:
            return element
    return None  # nothing found


def merge(old_config, new_config):
    for section in old_config.sections:
        print('Merging section '+section.name+' from old cfg')
        section_new = new_config.section_by_name(section.name)
        if not section_new:
            new_config.sections.append(section)
        else:  # same section exists so just copy contents
            for element in section.contents.elements:
                if type(element) == grammar2.SettingLine and str(element.name) == 'proj_path':
                    continue
                section_new.contents.elements.append(element)




class DistPeerObserver():
    """ Intention: DistPeerObserver stores all "distPort" entries along lists where they appeared. """
    def __init__(self):
        self.data = []
    def __call__(self,x,l):
        if type(x) == grammar2.SettingLine and x.name.name == "distPeer":
            self.data.append([x,l])




def main(old_fn, new_fn, result_fn, args):
    print('Started merge tool, old file: '+str(old_fn))
    yes_no_selector = yes_or_no
    if args.silently_merge_duplicated_sections:
        yes_no_selector = lambda question: 'y'
    global verbose
    verbose = args.verbose
    try:
        conf_old = grammar2.open_configuration(old_fn, yes_no_selector)
    except Exception as e:
        print('While opening '+old_fn+':')
        raise e
    try:
        conf_new = grammar2.open_configuration(new_fn, yes_no_selector)
    except Exception as e:
        print('While opening '+new_fn+':')
        raise e

    # "copy" the originals in the base directory of result
    writeout(conf_old, result_fn+'.old')
    writeout(conf_new, result_fn+'.new')

    preprocess_old_config(conf_old)
    if args.create_post_file:
        writeout(conf_old, old_fn+'.post')

    if args.ignore_old_ports:
        print('--ignore_old_ports is chosen: looking for portXXX entries in old config file')
        general_section = conf_old.section_by_name('general')
        if general_section:
            remove_given_keys_from_section(general_section, ['eventPort', 'dataPort', 'pmonPort'])
        dist_section = conf_old.section_by_name('dist')
        if dist_section:
            remove_given_keys_from_section(dist_section, ['distPort'])
    else:
        # dataPort, eventPort, pmonPort, distPort
        for section_and_key in [('dist','distPort'),('general','eventPort'),('general','dataPort'),('general','pmonPort')]:
            key = section_and_key[1]
            entry_in_old = find_setting_line(conf_old, section_and_key[0], key)
            entry_in_new = find_setting_line(conf_new, section_and_key[0], key)
            if entry_in_new:
                if entry_in_old:
                    print('WARNING: '+key+' is both in new and old config files; the value from old one will be taken.')
                else:
                    print('WARNING: '+key+' is in new config file; it will be dropped thus a default will be used')
                conf_new.section_by_name(section_and_key[0]).contents.elements.remove(entry_in_new)
    print('Removing all trailing comments from new config file')
    remove_all_trailing_comments(conf_new)

    merge(conf_old, conf_new)

    # fix double-slashes and trailing slashes of proj_paths from new(template) file
    new_general_paths = [x for x in conf_new.section_by_name('general').contents.elements if type(x)==grammar2.SettingLine and str(x.name) == 'proj_path']
    for path in new_general_paths:
        # pdb.set_trace()
        fixed = fix_path(path.arg_value)
        if fixed != path.arg_value:
            print('WARNING: corrected format of path {0}'.format(path.arg_value))
            path.arg_value = fixed

    # merge proj_paths from old to new such that there are no duplicates, and they are just before other proj_paths
    # use insert to see where is the first proj_path in new
    new_general = conf_new.section_by_name('general')
    for i in range(0, len(new_general.contents.elements)):
        elm = new_general.contents.elements[i]
        # todo: if it is a settingline with projpath means that at this index we should insert remaining proj paths
        if type(elm) == grammar2.SettingLine and str(elm.name) == 'proj_path':
            print('Found first proj_path at position {0}'.format(i))
            if verbose:
                print('At this step, the section looks like:')
                print(pypeg2.compose(new_general))
            # now find all proj_paths from the old
            old_general = conf_old.section_by_name('general')
            old_general_paths = [x for x in old_general.contents.elements if type(x)==grammar2.SettingLine and str(x.name) == 'proj_path']
            for p in old_general_paths:
                print('Processing proj_path {0} from the old config file'.format(p.arg_value))
                p.arg_value = fix_path(p.arg_value)
                if p not in new_general.contents.elements:
                    new_general.contents.elements.insert(i, p)
                    i += 1
                else:
                    print('Not merging duplicate proj_path: '+str(p.arg_value))
                if verbose:
                    print('Now the section looks like:')
                    print(pypeg2.compose(new_general))

            break



    # in all sections, find duplicate keys allowing only some otherwise quit (see: ACDCS-1392 )
    for section in conf_new.sections:
        allowed_duplicates_list = allowed_duplicates_any_section
        key = section.name.name
        if key in allowed_duplicates:
            allowed_duplicates_list += allowed_duplicates[key]
        match_driver_section_name = regex_driver_section_name.match(key)
        if match_driver_section_name:
            driver = match_driver_section_name.group('driver_name')  # key driver_name is a named argument in the regex
            if driver in allowed_duplicates_per_manager:
                allowed_duplicates_list += allowed_duplicates_per_manager[driver]


        (ok,duplicates) = utilities.check_duplicates(section, allowed_duplicates_list)
        if not ok:
            print('Abandoning merge because section ['+str(section.name)+'] has duplicates which are forbidden')
            sys.exit(1)

    # remove all distPeer = xxxxx 4777 with a warning (see: ACDCS-1392)
    dist_section = conf_new.section_by_name('dist')
    dpo = DistPeerObserver ()
    grammar2.visit_all_store_container(dist_section, dpo, conf_new.sections)
    for dp in dpo.data:
        print(type(dp[0]))
        if '4777' in dp[0].arg_value:
            print('WARNING: Found port "4777" in the distPeer, declared as: << '+dp[0].arg_value+' >> probably an artifact of UNICOS framework, Im removing it!')
            dp[1].remove(dp[0])


    writeout(conf_new, result_fn)
    print('Result written out as '+result_fn)

if __name__ == "__main__":


    # parse input
    parser = argparse.ArgumentParser(description='Merge two WinCCOA project config files')
    #parser.add_argument('integers', metavar='N', type=int, nargs='+',
    #                    help='an integer for the accumulator')
    parser.add_argument('--ignore_old_ports', dest='ignore_old_ports', action='store_true')
    parser.add_argument('--silently_merge_duplicated_sections', dest='silently_merge_duplicated_sections', action='store_true')
    parser.add_argument('--create_post_file', dest='create_post_file', action='store_true')
    parser.add_argument('--verbose', dest='verbose', action='store_true')
    parser.add_argument('old_fn')
    parser.add_argument('new_fn')
    parser.add_argument('result_fn')
    args = parser.parse_args()

    main(args.old_fn, args.new_fn, args.result_fn, args)
