
import re
import copy
from pypeg2 import *

# plain_comment is any line starting with hash, excluding #begin, #end, as they mark components subblocks
plain_comment = re.compile(r"#(?!begin|end)[^\n]*")
anything_up_to_new_line = re.compile(r"[^\n]*")
newline = re.compile(r'\n+')
whitespace_sameline = re.compile(r'[ \t]+')
restline_before_comment = re.compile(r'[^#\n]*')


class SettingLine(str):
    grammar = name(), blank, '=', blank, attr('arg_value', restline_before_comment), attr('comment', optional(plain_comment)), newline
    def __eq__(self, other):
        return type(self)==type(other) and self.name==other.name and self.arg_value==other.arg_value

class CommentLine(Concat):
    grammar = plain_comment, newline

class Comment(str):
    grammar = plain_comment

class CommentBlock(List):
    grammar = None  # will be assigned later, because recurrsion is necessary (and Python cant refer to the class itself in its body)
class Block(List):
    grammar = attr('elements', maybe_some( [CommentBlock, SettingLine, CommentLine] ))


class Section(Concat):
    grammar = '[', name(), ']', attr('comment', optional(Comment)), newline, attr('contents', Block)

    def all_blocks(self):
        return [x for x in self.contents.elements if type(x)==CommentBlock]

    def blocks_by_component_name(self, name):
        return [x for x in self.contents.elements if type(x)==CommentBlock and x.component_name_begin==name]

    def __eq__(self, other):
        # TODO: elaborate more on comparison here

        return self.name == other.name

class Configuration(List):
    grammar = attr('sections', maybe_some( Section ))
    def sections_by_name(self, name):
        return [x for x in self.sections if x.name==name]
    def section_by_name(self, name):
        sections_matching = self.sections_by_name(name)
        if len(sections_matching) < 1:
            return None
        else:
            return sections_matching[0]

# TODO: comment block elements should be accessible as  attr('elements', maybe_some( [CommentBlock, SettingLine, CommentLine] ))

def initialize_grammar():
    CommentBlock.grammar = '#begin', blank, attr('component_name_begin', word), anything_up_to_new_line, newline, attr('elements', maybe_some([SettingLine, CommentBlock, CommentLine], )), '#end', blank, attr('component_name_end', word), anything_up_to_new_line, newline


# List of Settings works fine, now trying the optional part

def merge_duplicated_section(sections):
    for section_i in range(0, len(sections)):
        for j in range(section_i+1, len(sections)):
            if sections[j].name == sections[section_i].name:
                print ('Merging duplicated section ['+sections[section_i].name+'] #'+str(j)+' to #'+str(section_i))
                sections[section_i].contents.elements.extend(sections[j].contents.elements)
                sections.pop(j)
                return True

def check_duplicated_section(configuration):
    for section in configuration.sections:
        num = len(configuration.sections_by_name(section.name))
        if num != 1:
            return [True, section.name]
    return [False, None]


def open_configuration(file_name, ask_function=None):
    initialize_grammar()
    conf_f = open(file_name, 'r')
    conf_text = conf_f.read()
    if conf_text[-1] != '\n':
        conf_text += '\n'
    configuration = parse( conf_text, Configuration, whitespace=whitespace_sameline )
    # ensure section names are unique
    while True:
        result=check_duplicated_section(configuration)
        if not result[0]:
            break
        if not ask_function:
            raise Exception('Section '+result[1]+' is present more than once.')
        else:
            reply = ask_function('Found duplicate section ['+result[1]+']. Would you like to merge it to the first occurence?')
            if reply:
                merge_duplicated_section(configuration.sections)


    return configuration


# This is a generic visitor implementation, will try to walk all reachable nodes and invoke handler on them
def visit_all(root, handler):
    handler(root)
    if type(root) == Configuration:
        for section in root.sections:
            visit_all( section, handler)
    if type(root) == Section:
        visit_all( root.contents, handler)
    if type(root) == Block or type(root) == CommentBlock:
        for element in root.elements:
            visit_all( element, handler)


def visit_all_store_container(node, handler, parent):
    """ A variant of generalized visitor, which runs handler with visited element along the collection it is contained. """
    handler(node, parent)
    if type(node) == Configuration:
        for section in node.sections:
            visit_all_store_container( section, handler, node.sections)
    if type(node) == Section:
        visit_all_store_container( node.contents, handler, node)
    if type(node) == Block or type(node) == CommentBlock:
        for element in node.elements:
            visit_all_store_container( element, handler, node.elements)
